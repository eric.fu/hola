#!/bin/bash
init(){
    set -e
    AU_REPOSITORY_URL=registry-intl-vpc.ap-southeast-2.aliyuncs.com # Internal VPC URL

    AU_IMAGE_TAG=$AU_REPOSITORY_URL/sdce-production/$REPOSITORY_NAME:$CI_COMMIT_SHORT_SHA
    AU_LATEST_TAG=$AU_REPOSITORY_URL/sdce-production/$REPOSITORY_NAME:latest
}


build(){
    docker login --username=$ALI_REPOSITORY_USERNAME $AU_REPOSITORY_URL --password=$ALI_REPOSITORY_PASSWORD

    echo $APP

    if [ $APP == "id3global" ];then
        docker build -f ./Dockerfile-python -t $AU_IMAGE_TAG .
    else
        docker build --build-arg BUILD_URL=$BUILD_URL --build-arg GIT_USER=$GIT_USER --build-arg GIT_ASKPASS=$GIT_ASKPASS -t $AU_IMAGE_TAG .
    fi

    docker push $AU_IMAGE_TAG
    docker tag $AU_IMAGE_TAG $AU_LATEST_TAG
    docker push $AU_LATEST_TAG
}


main(){
    init
    build
}

main $@